<?php

class Building {
  protected $name;
  protected $floor;
  protected $address;

  function __construct($name, $floor, $address){
    $this->name = $name;
    $this->floor = $floor;
    $this->address = $address;
  }
  // name getter setter
  function getName(){
    return $this->name;
  }
  function setName($name){
    $this->name = $name;
  }
  // floor getter setter
  function getFloor(){
    return $this->floor;
  }
  function setFloor($floor){
    $this->floor = $floor;
  }
  // address getter setter
  function getAddress(){
    return $this->address;
  }
  function setAddress($address){
    $this->address = $address;
  }
}

class Condominium extends Building {

  function __construct($name, $floor, $address){
    $this->name = $name;
    $this->floor = $floor;
    $this->address = $address;
  }

  // name getter setter
  function getName(){
    return $this->name;
  }
  function setName($name){
    $this->name = $name;
  }
  // floor getter setter
  function getFloor(){
    return $this->floor;
  }
  function setFloor($floor){
    $this->floor = $floor;
  }
  // address getter setter
  function getAddress(){
    return $this->address;
  }
  function setAddress($address){
    $this->address = $address;
  }
}
