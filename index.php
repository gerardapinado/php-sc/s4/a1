<<?php require_once './code.php'  ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>S4: Activity</title>
  </head>
  <body>

    <div class="">
      <h1>Building</h1>
      <?php
        $bldg1 = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");
        echo "<p>The name of the building is ".$bldg1->getName().".</p>";
        echo "<p>The ".$bldg1->getName()." has ".$bldg1->getFloor()." floors.</p>";
        echo "<p>The ".$bldg1->getName()." is located at ".$bldg1->getAddress().".</p>";
        $bldg1->setName("Caswynn Complex");
        echo "<p> The name of the building has been changed to " .$bldg1->getName(). ".</p>";
       ?>
      <h1>Condominium</h1>
      <?php
        $condo1 = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");
        echo "<p>The name of the building is ".$condo1->getName().".</p>";
        echo "<p>The ".$condo1->getName()." has ".$condo1->getFloor()." floors.</p>";
        echo "<p>The ".$condo1->getName()." is located at ".$condo1->getAddress().".</p>";
        $condo1->setName("Enzo Tower");
        echo "<p> The name of the building has been changed to " .$condo1->getName(). ".</p>";
       ?>
    </div>

  </body>
</html>
